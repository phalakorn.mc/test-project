import { InputNumber, Select } from 'antd';
import { useState, useEffect} from "react";
import axios from 'axios';
import 'antd/dist/antd.min.css';
import './App.scss';

function App() {
  const [valueInput,setValueInput] = useState(1);
  const [valueSelect,setValueSelect] = useState(1);
  const [valueColumn3,setValueColumn3] = useState(false);
  const { Option } = Select;
  const onPressEnter = (e) => {
    if (e.target.value < 0) {
      setValueInput(1);
    }else{
      setValueInput(Math.round(e.target.value));
    }
  }
  const onSelect = (value) => {
    setValueSelect(value);
  }

  useEffect(()=>{
    const calculation = () =>{
      if(valueSelect === 1){
        setValueColumn3(isPrime(valueInput));
      }else{
        setValueColumn3(fibonacci(valueInput));
      }
    }

    const isPrime = (num) => {
      for(var i = 2; i < num; i++)
        if(num % i === 0) return false;
      return num > 1;
    }

    const fibonacci = (query, count = 1, last = 0) => {
        if(count < query){
          return fibonacci(query, count+last, count);
        };
        if(count === query){
          return true;
        }
        return false;
    };
    calculation();
  },[valueInput,valueSelect]);

  return (
    <div className="App">
      <div className='block-columns'>
        <div className='column first'>
          <InputNumber min={1} onPressEnter={(e)=>onPressEnter(e)} value={valueInput}  />
        </div>
        <div className='column middle'>
          <Select defaultValue="isPrime" style={{ width: 100 }} onSelect={onSelect}>
            <Option value={1}>isPrime</Option>
            <Option value={2}>isFibonacci</Option>
          </Select>
        </div>
        <div className='column right'>
          {valueColumn3.toString()}
        </div>
      </div>
    </div>
  );
}

export default App;
